
//   1)Екранування – це заміна в тексті спеціальних символів на відповідні текстові підстановки тоді,
//   коли необхідно використовувати такий символ як «звичайний символ мови».
//   Екрануючий символ (\) повідомляє інтерпретатору, що символ,
//   що слідує за ним, повинен сприйматися як звичайний символ.


//    2)function declaration, function expression, стрелочные функции.


//   3)Hoisting або підняття  –  це можливість отримувати доступ до функцій та змінних до того,
//   як вони були створені. Це механізм відноситься лише до оголошення функцій та змінних.
"use strict"


function createNewUser() {
   const newUser = new Object();
   newUser.firstName = prompt("Enter you firstName");
   newUser.lastName = prompt("Enter you lastName");
   newUser.birthDay = prompt("Enter your Happy Birthday, `dd.mm.yyyy`");
   newUser.getLogin = function () {
      return `${(newUser.firstName.charAt(0) + newUser.lastName).toLowerCase()}`;
   };


   newUser.getAge = function () {
      return `${new Date().getFullYear() - newUser.birthDay.slice(6)}`;
   };


   newUser.getPassword = function () {
      return `${newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthDay.slice(6)}`;
   }
   return newUser;

}


const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

