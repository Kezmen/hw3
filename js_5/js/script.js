//1) віін зберігає в собі значення які можна змінювати убирати або добавляти
//це особливий тип даних або функції для роботи із функціями
//2)Він може мати будь-який тип даних.
//3)Це означає, що він зберігає у собі посилання на обєкт, а не значення.


"use strict"


function creatNewUser() {
   const newUser = new Object();
   newUser.firstName = prompt("Enter your name: ");
   while (newUser.firstName === "" || !isNaN(newUser.firstName)) {
      newUser.firstName = prompt("Enter your name again: ", newUser.firstName)
   }
   newUser.lastName = prompt("Enter your lastName: ");
   while (newUser.lastName === "" || !isNaN(newUser.lastName)) {
      newUser.lastName = prompt("Enter your lastName again: ", newUser.lastName)
   }

   newUser.getLogin = function () {
      return `${(newUser.firstName + " " + newUser.lastName).toLowerCase()}`;
   }

   return newUser;
}

let user = creatNewUser();
console.log(user.getLogin());


